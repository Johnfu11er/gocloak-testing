ssl:
	mkcert --key-file dev/tls/server.key --cert-file dev/tls/server.crt "*.test.dev"
	cp "$(shell mkcert -CAROOT)"/rootCA.pem dev/tls