# GOCLOAK Testing

### Startup
- Clone this repo
- Generate SSL cert/key
  - Navigate to `gocloak-testing` directory
  - Run `make ssl`
- Start containers
  - Start the Docker Desktop application
  - Navigate to `gocloak-testing` directory
  - Start the containers with:
    ```
    docker compose up -d
    ```
    - Run `docker ps` to make sure that all of the conatiners are running
    - If any contianer, other than the oauth2-proxy, is restarting, shut down the containers with `docker compose down -v` and restart with `docker compose up -d`
- Start the client
  - In a new terminal, navigate to `gocloak-testing/client` directory
  - Start the client with `npm start`
- Start the server
  - In a new terminal, navigate to `gocloak-testing/server` directory
  - Start the client with `go run .`
- Go to the application
  - Open your web browser and go to [https://my-app.test.dev](https://my-app.test.dev)
  - Login to keycloak with:
    - Username: `user`
    - Password: `password`
  - You should be redirected to the https://my-app.test.dev webpage

### Using the application
- Create a new mission by entering a mission name and clicking `Create Mission`
  - Two new groups are also created and can be viewed by clicking on the `Get Groups` button


# Major Changes to get this working
### Oauth2-proxy
- Gocloak requires the use of an access token for its connection to keycloak
- Oauth2-proxy must pass the access token from keycloak when the user authenticates
- This configuration must be added to the `oauth2-proxy.cfg` file to pass the access token:
  ```
  set_xauthrequest= "true"
  pass_access_token= "true"
  ```

### NGINX
- Once Oauth2-proxy has passed on the access token from keycloak, NGINX must set the access token as a value in the header for the key `X-Access-Token` through the setting the following commands on the `/api` endpoint in the `default.conf.template` file:
  ```
  auth_request_set    $token $upstream_http_x_auth_request_access_token;
  proxy_set_header    X-Access-Token $token;
  ```
- Here is a full example of the NGINX `/api` location:
  ```
  location /api {        
    proxy_pass          http://host.docker.internal:8000;

    proxy_set_header    Host $host;
    proxy_set_header    Upgrade $http_upgrade;
    proxy_set_header    X-Forwarded-Proto $scheme;
    proxy_set_header    Connection $connection_upgrade;
    
    auth_request_set    $id_token $upstream_http_authorization;
    proxy_set_header    Authorization $id_token;
  
    auth_request        /oauth2/auth;
    error_page          401 = /oauth2/start;

    auth_request_set    $token $upstream_http_x_auth_request_access_token;
    proxy_set_header    X-Access-Token $token;
  }
  ```

### Keycloak permissions
- To gain access to the admin cli functions in Keyclaok, a user must have the appropriate permissions set from the `realm-management` client
- The easiest way to accomplish this is:
  1. Create a new realm group.
  2. Assign the client role `realm-admin` from the `realm-management` client. This will add all of the inherited roles under the `realm-management` client.  This may be too many permissions depending on your specific needs.  In which case, you can only add the individual `realm-management` client roles that you need.
  3. Add your user to the new realm group that you have just created and assigned client roles to.
  4. If your user has an existing session, they will need to end that session and start a new one in order to obtain a new JWT with the appropriate levels of permissions