import { useState } from "react"
import ListMissions from "./ListMissions"
import ListUsers from "./ListUsers"
import ListClients from "./ListClients"
import ListGroups from "./ListGroups"

let initialMissions = [
  {ID: 0, Name: "Mission 0"},
  {ID: 1, Name: "Mission 1"},
  {ID: 2, Name: "Mission 2"},
]

let nextId = initialMissions.length

const App = () => {
  const [ missions, setMissions ] = useState(initialMissions)
  const [ users, setUsers ] = useState([])
  const [ clients, setClients ] = useState([])
  const [ groups, setGroups ] = useState([])
  

  const handleCreateMission = async (missionName) => {
    setMissions([
      ...missions,
      {
        ID: nextId++,
        Name: missionName
      }
    ])
    console.log(missions)
    await handleCreateNewUserGroup(missionName)
    await handleCreateNewAdminGroup(missionName)
  }
  

  const handleCreateNewUserGroup = async (missionName) => {
    console.log("creating restricted user group:", "restricted-" + missionName + "-user")
    await fetch("https://my-app.test.dev/api/create-realm-group", {
      method: "POST",
      headers: {
        "Content-Type": "applicaton/json"
      },
      body: JSON.stringify({"name": missionName + "-user"}),
    })
    console.log("group created")
  }
  
  
  const handleCreateNewAdminGroup = async (missionName) => {
    console.log("creating restricted admin group:", "restricted-" + missionName + "-admin")
    await fetch("https://my-app.test.dev/api/create-realm-group", {
      method: "POST",
      headers: {
        "Content-Type": "applicaton/json"
      },
      body: JSON.stringify({"name": missionName + "-admin"}),
    })
    console.log("group created")
  }


  const handleGetUsers = async () => {
    console.log("getitng users")
    const response = await fetch("https://my-app.test.dev/api/get-users", { method: "GET" })
    const responseJSON = await response.json()
    setUsers(responseJSON)
    console.log(responseJSON)
  }
  
  
  const handleGetClients = async () => {
    const response = await fetch("https://my-app.test.dev/api/get-clients", { method: "GET" })
    const responseJSON = await response.json()
    setClients(responseJSON)
    console.log(responseJSON)
  }
  
  
  const handleGetGroups = async () => {
    const response = await fetch("https://my-app.test.dev/api/get-groups", { method: "GET" })
    const responseJSON = await response.json()
    setGroups(responseJSON)
    console.log(responseJSON)
  }
  

  const handleCreateRealmRole = async () => {
    await fetch("https://my-app.test.dev/api/create-realm-role", { method: "POST" })
  }


  const handleCreateClientRole = async () => {
    await fetch("https://my-app.test.dev/api/create-client-role", { method: "POST" })
  }


  return (
    <>

      <button onClick={handleCreateRealmRole}>Create Realm Role</button>
      <button onClick={handleCreateClientRole}>Create Client Role</button>
      <ListMissions
        onCreate={handleCreateMission}
        missions={missions}
      />
      <ListUsers
        users={users}
        onGetUsers={handleGetUsers}
      />
      <ListClients
        clients={clients}
        onGetClients={handleGetClients}
      />
      <ListGroups
        groups={groups}
        onGetGroups={handleGetGroups}
      />
    </>
  )
}

export default App