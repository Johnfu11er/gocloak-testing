const ListClients = ({ clients, onGetClients }) => {
  return (
    <>
      <h1>Clients</h1>
      <button onClick={onGetClients}>Get Clients</button>
      <ul>
        {clients.map(client => (
          <li key={client.id}>
            {client.clientId}
            <br/>
              id: {client.id} 
          </li>
        ))}
      </ul>
    </>
  )
}

export default ListClients