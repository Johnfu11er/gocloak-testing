const ListGroups = ({ groups, onGetGroups }) => {
  return (
    <>
      <h1>Groups</h1>
      <button onClick={onGetGroups}>Get Groups</button>
      <ul>
      {groups.map(group => (
        <li key={group.id}>{group.name}</li>
      ))}
      </ul>
    </>
  )
}

export default ListGroups