import { useState } from "react"

const ListMissions = ({ missions, onCreate }) => {
  
  return (
    <>
      <h1>Missions</h1>
      <CreateMission
        onCreate={onCreate}
        />
      <ul>
        {missions.map(mission => (
          <li key={mission.ID}>
            {mission.Name}
          </li>
        ))}
      </ul>
    </>
  )
}

const CreateMission = ({ onCreate }) => {
  const [ text, setText ] = useState("")
  
  return (
    <>
      <input
        value={text}
        onChange={e => setText(e.target.value)}
      />
      
      <button
        onClick={() => {
          if (!text) { return }
          onCreate(text)
          setText("")
        }}
        >
        Create Mission
      </button>
    </>
  )
} 

export default ListMissions