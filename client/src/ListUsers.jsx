const ListUsers = ({ users, onGetUsers }) => {
  return (
    <>
      <h1>Users</h1>
      <button onClick={onGetUsers}>Get Users</button>
      <br/>
      <ul>
        {users.map(user => (
          <li key={user.id}>
            {user.username}
            <button>add to a group</button>  
          </li>
        ))}
      </ul>
    </>
  )
}

export default ListUsers