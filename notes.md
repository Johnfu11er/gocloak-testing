- Get an access token keycloak for admin-cli client
- Add a user
- Get a list of users
- Client Role
  - Create
  - Delete
  - Assign to a user
  - Remove from a user
- Realm Role
  - Create
  - Delete
  - Assign to a user
  - Remove from a user

### Workflow
- Create a new restricted mission
  - Create a new restricted group in the app realm
  - Create a new restricted role for the app
  - Add the new role to the user that created the new mission
  - 


### MHUB Groups
- mhub-admin
  - mhub-admin
- muhb-mission-creator
  - mhub-mission-creator
- mhub-permission-granter
  - mhub-permission-granter

### Realm Groups
- Realm Admin
  - Needs various realm-management roles


# Actions
### Create a restricted mission
- Who: Realm-admin
- Actions:
  - MHUB
    - Creates a restricted mission in MHUB
  - Keycloak
    - Creates `{this} restricted mission admin group` with roles:
      - Delete {this} restricted mission
    - Creates `{this} restricted mission user group` with roles:
      - View {this} restricted mission
      - Edit {this} restricted mission

### Delete a restricted mission
- Who: Realm-admin

### Add a user to a restricted mission
- Who: Realm-admin