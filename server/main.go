package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	// "strings"

	"net/http"

	// "github.com/lestrrat-go/jwx/v2/jwt"
	"github.com/Nerzal/gocloak/v13"
	"github.com/gin-gonic/gin"
)

func main() {
  router := gin.Default()
	
	apiGroup := router.Group("/api")
  {
		apiGroup.GET("/get-users", getUsers)
		apiGroup.GET("/get-clients", getClients)
		apiGroup.GET("/get-groups", getGroups)
		apiGroup.POST("/create-realm-role", createRealmRole)
		apiGroup.POST("/create-client-role", createClientRole)
		apiGroup.POST("/create-realm-group", createRealmGroup)
	}

  router.Run(":8000")
}

func getAccessToken(c *gin.Context) string {
	// Parses access token from response header
	return c.Request.Header["X-Access-Token"][0]
}

func getIdToken(c *gin.Context) string {
	// Parses ID token from response header
	// The ID token is not valid for use with the Keycloak API calls
	fmt.Println(c.Request.Header["Authorization"][0])
	return c.Request.Header["Authorization"][0]
}


func getUsers(c *gin.Context) {
	// Returns a list of user objects from the keycloak server
	xAccessToken := getAccessToken(c)
	client := gocloak.NewClient("https://keycloak.test.dev")
	ctx := context.Background()

	fmt.Println("Getting users...")
	
	users, err := client.GetUsers(ctx, xAccessToken, "my-realm", gocloak.GetUsersParams{})
	if err != nil {
		panic(err)
	} else {
		fmt.Println(users)
	}

	c.IndentedJSON(http.StatusOK, users)
}


func getClients(c *gin.Context) {
	xAccessToken := getAccessToken(c)

	client := gocloak.NewClient("https://keycloak.test.dev")
	ctx := context.Background()
	realm := "my-realm"

	clients, err := client.GetClients(ctx, xAccessToken, realm, gocloak.GetClientsParams{})
	if err != nil {
		panic(err)
	} else {
		fmt.Println(clients)
	}

	c.IndentedJSON(http.StatusOK, clients)
}


func getGroups(c *gin.Context) {
	xAccessToken := getAccessToken(c)

	client := gocloak.NewClient("https://keycloak.test.dev")
	ctx := context.Background()
	realm := "my-realm"

	groups, err := client.GetGroups(ctx, xAccessToken, realm, gocloak.GetGroupsParams{})
	if err != nil {
		panic(err)
	} else {
		fmt.Println(groups)
	}

	c.IndentedJSON(http.StatusOK, groups)
}


func createRealmRole(c *gin.Context) {
	// Creates a new realm role object in the keycloak server
	xAccessToken := getAccessToken(c)

	client := gocloak.NewClient("https://keycloak.test.dev")
	ctx := context.Background()
	realm := "my-realm"

	newRole := gocloak.Role{
		ID: gocloak.StringP("Test-Role-Id"),
		Name: gocloak.StringP("Test-Role-Name"),
		ScopeParamRequired: gocloak.BoolP(false),
		Composite: gocloak.BoolP(false),
		ClientRole: gocloak.BoolP(false),
		ContainerID: gocloak.StringP("Test-Role-Container-ID"),
		Description: gocloak.StringP("Test-Role-Description"),
	}

	roleName, err := client.CreateRealmRole(ctx, xAccessToken, realm , newRole)
	if err != nil {
		panic(err)
	} else {
		fmt.Println(roleName)
	}
}


func createClientRole(c *gin.Context) {
	xAccessToken := getAccessToken(c)

	client := gocloak.NewClient("https://keycloak.test.dev")
	ctx := context.Background()
	realm := "my-realm"

	newRole := gocloak.Role{
		ID: gocloak.StringP("Test-Client-Role-Id"),
		Name: gocloak.StringP("Test-Client-Role-Name"),
		ScopeParamRequired: gocloak.BoolP(false),
		Composite: gocloak.BoolP(false),
		ClientRole: gocloak.BoolP(true),
		ContainerID: gocloak.StringP("Test-Client-Role-Container-ID"),
		Description: gocloak.StringP("Test-Client-Role-Description"),
	}

	roleName, err := client.CreateClientRole(ctx, xAccessToken, realm, "c37f62e5-8dfe-4bfd-bfdd-105404ea9635", newRole)
	if err != nil {
		panic(err)
	} else {
		fmt.Println(roleName)
	}
}

func createRealmGroup(c *gin.Context) {
	// Creates a new realm role object in the keycloak server
	xAccessToken := getAccessToken(c)

	client := gocloak.NewClient("https://keycloak.test.dev")
	ctx := context.Background()
	realm := "my-realm"

	newGroupName := parseBody(c)["name"].(string)

	newGroup := gocloak.Group{
		Name: gocloak.StringP("restricted-" + newGroupName),
	}

	roleName, err := client.CreateGroup(ctx, xAccessToken, realm , newGroup)
	if err != nil {
		panic(err)
	} else {
		fmt.Println(roleName)
	}
	fmt.Println(c.Request.Body)
	
}

func parseBody(c *gin.Context) map[string]interface{} {
	bodyAsByteArray, err := io.ReadAll(c.Request.Body)
	if err != nil {
		panic(err)
	}

	jsonMap := make(map[string]interface{})
	json.Unmarshal(bodyAsByteArray, &jsonMap)
	return jsonMap
}